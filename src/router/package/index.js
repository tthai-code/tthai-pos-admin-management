export default {
  path: '/package',
  component: () => import('@/views/package/index.vue'),
  children: [
    {
      path: '',
      name: 'PackageList',
      component: () => import('@/views/package/page/PackageList.vue'),
      meta: {
        appBar: {
          title: 'Package',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-package-variant-closed' }
        }
      }
    },
    {
      path: 'add',
      name: 'PackageAdd',
      component: () => import('@/views/package/page/PackageAdd.vue'),
      meta: {
        appBar: {
          title: 'Package',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-package-variant-closed' }
        }
      }
    },
    {
      path: 'edit/:id',
      name: 'PackageEdit',
      component: () => import('@/views/package/page/PackageEdit.vue'),
      meta: {
        appBar: {
          title: 'Package',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-package-variant-closed' }
        }
      }
    }
  ]
}
