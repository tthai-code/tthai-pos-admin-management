export default {
  path: '/equipment',
  component: () => import('@/views/equipment/index.vue'),
  children: [
    {
      path: 'super-admin',
      name: 'BillList',
      component: () => import('@/views/equipment/pages/EquipmentSuperAdminList.vue'),
      meta: {
        appBar: {
          title: 'Equipment',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-ballot-outline' }
        }
      }
    },
    {
      path: 'add',
      name: 'EquipmentAdd',
      component: () => import('@/views/equipment/pages/EqiupmentAdd.vue'),
      meta: {
        appBar: {
          title: 'Equipment',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-ballot-outline' }
        }
      }
    },
    // {
    //   path: 'detail/:equipmentId',
    //   name: 'BillDetail',
    //   component: () => import('@/views/equipment/page/BillDetail.vue'),
    //   meta: {
    //     appBar: {
    //       title: 'Restaurant',
    //       // FIXME: mdi-account-circle-outline
    //       icon: { icon: 'mdi-store' }
    //     }
    //   }
    // },
    {
      path: 'add/:equipmentId',
      name: 'EquipmentEdit',
      component: () => import('@/views/equipment/pages/EqiupmentAdd.vue'),
      meta: {
        appBar: {
          title: 'Equipment',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-ballot-outline' }
        }
      }
    }
    // {
    //   path: 'report',
    //   name: 'BillReport',
    //   component: () => import('@/views/equipment/page/BillReport.vue'),
    //   meta: {
    //     appBar: {
    //       title: 'Restaurant',
    //       // FIXME: mdi-account-circle-outline
    //       icon: { icon: 'mdi-store' }
    //     }
    //   }
    // }
  ]
}
