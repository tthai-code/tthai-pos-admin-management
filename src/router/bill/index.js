export default {
  path: '/bill',
  component: () => import('@/views/bill/index.vue'),
  children: [
    {
      path: 'super-admin',
      name: 'BillList',
      component: () => import('@/views/bill/pages/SuperAdminBillList.vue'),
      meta: {
        appBar: {
          title: 'Billing',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-store' }
        }
      }
    },
    {
      path: 'user',
      name: 'BillList',
      component: () => import('@/views/bill/pages/UserBillList.vue'),
      meta: {
        appBar: {
          title: 'Billing',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-store' }
        }
      }
    }
    // {
    //   path: 'add',
    //   name: 'BillAdd',
    //   component: () => import('@/views/bill/page/BillAdd.vue'),
    //   meta: {
    //     appBar: {
    //       title: 'Restaurant',
    //       // FIXME: mdi-account-circle-outline
    //       icon: { icon: 'mdi-store' }
    //     }
    //   }
    // },
    // {
    //   path: 'detail/:billId',
    //   name: 'BillDetail',
    //   component: () => import('@/views/bill/page/BillDetail.vue'),
    //   meta: {
    //     appBar: {
    //       title: 'Restaurant',
    //       // FIXME: mdi-account-circle-outline
    //       icon: { icon: 'mdi-store' }
    //     }
    //   }
    // },
    // {
    //   path: 'add/:billId',
    //   name: 'BillEdit',
    //   component: () => import('@/views/bill/page/BillAdd.vue'),
    //   meta: {
    //     appBar: {
    //       title: 'Restaurant',
    //       // FIXME: mdi-account-circle-outline
    //       icon: { icon: 'mdi-store' }
    //     }
    //   }
    // },
    // {
    //   path: 'report',
    //   name: 'BillReport',
    //   component: () => import('@/views/bill/page/BillReport.vue'),
    //   meta: {
    //     appBar: {
    //       title: 'Restaurant',
    //       // FIXME: mdi-account-circle-outline
    //       icon: { icon: 'mdi-store' }
    //     }
    //   }
    // }
  ]
}
