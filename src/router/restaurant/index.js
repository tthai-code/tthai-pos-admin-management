// export default {
//   path: '/restaurant',
//   component: () => import('@/views/restaurant/index.vue'),
//   children: [
//     {
//       path: 'setting',
//       name: 'MenuList',
//       component: () => import('@/views/restaurant/pages/RestaurantSetting.vue'),
//       meta: {
//         appBar: {
//           title: 'Restaurant Setting',
//           // FIXME: mdi-stool
//           icon: { icon: 'mdi-domain' }
//         }
//       }
//     }
//   ]
// }

export default {
  path: '/restaurant',
  component: () => import('@/views/restaurant/index.vue'),
  children: [
    {
      path: '',
      name: 'RestaurantList',
      component: () => import('@/views/restaurant/pages/RestaurantList.vue'),
      meta: {
        appBar: {
          title: 'Restaurant',
          icon: { icon: 'mdi-store' }
        }
      }
    },
    {
      path: 'detail/:id',
      name: 'RestaurantDetail',
      component: () => import('@/views/restaurant/pages/RestaurantDetail.vue'),
      meta: {
        appBar: {
          title: 'Restaurant',
          icon: { icon: 'mdi-store' }
        }
      }
    }
  ]
}
