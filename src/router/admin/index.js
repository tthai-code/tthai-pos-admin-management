export default {
  path: '/admin',
  component: () => import('@/views/admin/index.vue'),
  children: [
    {
      path: '',
      name: 'AdminList',
      component: () => import('@/views/admin/page/AdminList.vue'),
      meta: {
        appBar: {
          title: 'User Admin',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-card-account-details-outline' }
        }
      }
    },
    {
      path: 'add',
      name: 'AdminAdd',
      component: () => import('@/views/admin/page/AdminAdd.vue'),
      meta: {
        appBar: {
          title: 'User Admin',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-card-account-details-outline' }
        }
      }
    },
    {
      path: 'add/:adminId',
      name: 'AdminEdit',
      component: () => import('@/views/admin/page/AdminAdd.vue'),
      meta: {
        appBar: {
          title: 'User Admin',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-card-account-details-outline' }
        }
      }
    }
  ]
}
