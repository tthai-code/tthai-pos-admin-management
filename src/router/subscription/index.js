export default {
  path: '/subscription',
  component: () => import('@/views/subscription/index.vue'),
  children: [
    {
      path: 'user',
      name: 'subscription',
      component: () => import('@/views/subscription/pages/UserSubscriptionList.vue'),
      meta: {
        appBar: {
          title: 'Subscription',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-pin' }
        }
      }
    }

  ]
}
