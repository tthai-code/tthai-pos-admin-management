export default {
  path: '/user',
  component: () => import('@/views/user/index.vue'),
  children: [
    {
      path: '',
      name: 'UserList',
      component: () => import('@/views/user/page/UserList.vue'),
      meta: {
        appBar: {
          title: 'User',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-account-circle-outline' }
        }
      }
    },
    {
      path: 'add',
      name: 'UserAdd',
      component: () => import('@/views/user/page/UserAdd.vue'),
      meta: {
        appBar: {
          title: 'User',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-account-circle' }
        }
      }
    },
    {
      path: 'add/:userId',
      name: 'UserEdit',
      component: () => import('@/views/user/page/UserAdd.vue'),
      meta: {
        appBar: {
          title: 'User',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-account-circle' }
        }
      }
    }
  ]
}
