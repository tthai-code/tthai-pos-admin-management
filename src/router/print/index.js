export default {
  path: '/print',
  component: () => import('@/views/print/index.vue'),
  children: [
    {
      path: 'invoice/:billingId',
      name: 'InvoicePrint',
      component: () => import('@/views/print/pages/Invoice.vue'),
      meta: {
        layout: 'blank'
      }
    },
    {
      path: 'ach-file/:achfileId',
      name: 'ACHFilePrint',
      component: () => import('@/views/print/pages/AchFile.vue'),
      meta: {
        layout: 'blank'
      }
    }
  ]
}
