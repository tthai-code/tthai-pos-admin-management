export default {
  path: '/register',
  component: () => import('@/views/register/index.vue'),
  children: [
    {
      path: '',
      name: 'Register',
      component: () => import('@/views/register/pages/RegisterPage.vue'),
      meta: {
        layout: 'blank'
      }
    }

  ]
}
