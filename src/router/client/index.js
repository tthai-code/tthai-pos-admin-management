export default {
  path: '/client',
  component: () => import('@/views/client/index.vue'),
  children: [
    {
      path: '',
      name: 'ClientList',
      component: () => import('@/views/client/page/ClientList.vue'),
      meta: {
        appBar: {
          title: 'Restaurant',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-store' }
        }
      }
    },
    {
      path: 'add',
      name: 'ClientAdd',
      component: () => import('@/views/client/page/ClientAdd.vue'),
      meta: {
        appBar: {
          title: 'Restaurant',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-store' }
        }
      }
    },
    {
      path: 'detail/:clientId',
      name: 'ClientDetail',
      component: () => import('@/views/client/page/ClientDetail.vue'),
      meta: {
        appBar: {
          title: 'Restaurant',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-store' }
        }
      }
    },
    {
      path: 'add/:clientId',
      name: 'ClientEdit',
      component: () => import('@/views/client/page/ClientAdd.vue'),
      meta: {
        appBar: {
          title: 'Restaurant',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-store' }
        }
      }
    },
    {
      path: 'report',
      name: 'ClientReport',
      component: () => import('@/views/client/page/ClientReport.vue'),
      meta: {
        appBar: {
          title: 'Restaurant',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-store' }
        }
      }
    }
  ]
}
