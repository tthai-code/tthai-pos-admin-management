export default {
  path: '/referral',
  component: () => import('@/views/referral/index.vue'),
  children: [
    {
      path: 'super-admin',
      name: 'ReferralSuperadminList',
      component: () => import('@/views/referral/pages/ReferralSuperadminList.vue'),
      meta: {
        appBar: {
          title: 'Referral',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-barcode' }
        }
      }
    }

  ]
}
