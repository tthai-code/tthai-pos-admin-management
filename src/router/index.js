import Vue from 'vue'
import VueRouter from 'vue-router'

import { getAccessToken } from '@/utils/auth'

import ReportRouter from './report'
import ClientRouter from './client'
import AdminRouter from './admin'
import Print from './print'
import Bill from './bill'
import Equipment from './equipment'
import Subscription from './subscription'
import Restaurant from './restaurant'
import Referral from './referral'
import Register from './register'
import Package from './package'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/restaurant'
  },
  {
    path: '/signin',
    name: 'SignInPage',
    component: () => import('@/views/auth/pages/SigninV2.vue'),
    meta: {
      layout: 'blank'
    }
  },
  {
    path: '/signout',
    name: 'SignOutPage',
    component: () => import('@/views/auth/pages/Signout.vue'),
    meta: {
      layout: 'blank'
    }
  },

  ReportRouter,
  ClientRouter,
  AdminRouter,
  Print,
  Bill,
  Equipment,
  Subscription,
  Restaurant,
  Referral,
  Register,
  Package
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

const publicRoute = ['SignInPage', 'SignOutPage']

router.beforeEach((to, from, next) => {
  const routeName = to.name
  if (routeName === 'InvoicePrint') {
    next()
  }
  if (publicRoute.some((pr) => pr === routeName)) {
    next()
  } else {
    const token = getAccessToken()
    if (!token) {
      router.replace({ name: 'SignOutPage' })
    } else {
      next()
    }
  }
  return next()
})

export default router
