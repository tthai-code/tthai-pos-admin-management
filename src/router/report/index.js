export default {
  path: '/report',
  component: () => import('@/views/report/index.vue'),
  children: [
    // {
    //   path: '',
    //   name: 'ReportHome',
    //   component: () => import('@/views/report/page/ReportHome.vue'),
    //   meta: {
    //     appBar: {
    //       title: 'Report',
    //       // FIXME: mdi-stool
    //       icon: { icon: 'mdi-chart-pie' }
    //     }
    //   }
    // }
    {
      path: '',
      name: 'ReportList',
      component: () => import('@/views/report/pages/ReportList.vue'),
      meta: {
        appBar: {
          title: 'Report',
          icon: { icon: 'mdi-store' }
        }
      }
    }
  ]
}
