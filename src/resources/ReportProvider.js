import { getAuthToken } from '../utils/auth'
import HttpRequest from './HttpRequest'

class ReportProvider extends HttpRequest {
  // getSellingReport (query) {
  //   this.setHeader(getAuthToken())

  //   return this.get('/orders/report', { ...query })
  // }

  // getTodayReport (query) {
  //   this.setHeader(getAuthToken())

  //   return this.get('/orders/report/today', { ...query })
  // }

  // getThisWeekReport (query) {
  //   this.setHeader(getAuthToken())

  //   return this.get('/orders/report/week', { ...query })
  // }

  // getThisMonthReport (query) {
  //   this.setHeader(getAuthToken())

  //   return this.get('/orders/report/month', { ...query })
  // }

  // getThisYearReport (query) {
  //   this.setHeader(getAuthToken())

  //   return this.get('/orders/report/year', { ...query })
  // }

  // getDateRangeReport (query) {
  //   this.setHeader(getAuthToken())

  //   return this.get('/orders/report/range', { ...query })
  // }

  // getReportById (id) {
  //   this.setHeader(getAuthToken())
  //   return this.get(`/orders/report/${id}`)
  // }

  // createReport (payload) {
  //   this.setHeader(getAuthToken())

  //   return this.post('/orders/report', { ...payload })
  // }

  // updateReport (id, payload) {
  //   this.setHeader(getAuthToken())
  //   return this.put(`/orders/report/${id}`, payload)
  // }

  // deleteReport (id) {
  //   this.setHeader(getAuthToken())
  //   return this.delete(`/orders/report?id=${id}`)
  // }

  // getClientReport (query) {
  //   this.setHeader(getAuthToken())
  //   return this.get('/orders/report/admin', query)
  // }

  getReportAdmin (query) {
    this.setHeader(getAuthToken())
    return this.get('/v1/admin/report/sale', { ...query })
  }
}

export default new ReportProvider()
