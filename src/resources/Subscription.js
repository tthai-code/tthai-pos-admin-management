import { getAuthToken } from '../utils/auth'
import HttpRequest from './HttpRequest'

class SubscriptionProvider extends HttpRequest {
  getSubscriptionByRestaurantId (restaurantId, query) {
    this.setHeader(getAuthToken())
    return this.get(`/v1/admin/subscription/${restaurantId}/restaurant`, query)
  }

  getSubscriptionById (id) {
    this.setHeader(getAuthToken())
    return this.get(`/v1/admin/subscription/${id}`)
  }

  createSubscription (restaurantId, payload) {
    this.setHeader(getAuthToken())
    return this.post(`/v1/admin/subscription/${restaurantId}`, payload)
  }

  updateSubscription (id, payload) {
    this.setHeader(getAuthToken())
    return this.put(`/v1/admin/subscription/${id}`, payload)
  }

  unSubscribe (id, payload) {
    this.setHeader(getAuthToken())
    return this.patch(`/v1/admin/subscription/${id}/unsubscribe`, payload)
  }

  subscribe (id, payload) {
    this.setHeader(getAuthToken())
    return this.patch(`/v1/admin/subscription/${id}/subscribe`, payload)
  }

  // deletePackage (id) {
  //   this.setHeader(getAuthToken())
  //   return this.delete(`/v1/admin/package/${id}`)
  // }
}

export default new SubscriptionProvider()
