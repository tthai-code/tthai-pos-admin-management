import { getAuthToken } from '../utils/auth'
import HttpRequest from './HttpRequest'

class RestaurantProvider extends HttpRequest {
  getRestaurant (query) {
    this.setHeader(getAuthToken())
    return this.get('/v1/admin/restaurant', query)
  }

  getRestaurantById (id) {
    this.setHeader(getAuthToken())
    return this.get(`/v1/admin/restaurant/${id}/info`)
  }

  getSubscriptionByRestaurant (restaurantId) {
    this.setHeader(getAuthToken())
    return this.get(`/v1/admin/subscription/${restaurantId}`)
  }

  getAchById (achId) {
    this.setHeader(getAuthToken())
    return this.get(`/v1/admin/restaurant/${achId}/ach`)
  }

  getBillingByRestaurantId (query) {
    this.setHeader(getAuthToken())
    return this.get('/v1/admin/bill/restaurant', query)
  }

  updateRestaurantCreditFee (restaurantId, payload) {
    this.setHeader(getAuthToken())
    return this.patch(`/v1/admin/restaurant/${restaurantId}/credit-fee`, payload)
  }

  updateRestaurantDebitFee (restaurantId, payload) {
    this.setHeader(getAuthToken())
    return this.patch(`/v1/admin/restaurant/${restaurantId}/debit-fee`, payload)
  }

  activateRestaurantById (restaurantId) {
    this.setHeader(getAuthToken())
    return this.patch(`/v1/admin/restaurant/${restaurantId}/activate`)
  }

  deActivateRestaurantById (restaurantId) {
    this.setHeader(getAuthToken())
    return this.patch(`/v1/admin/restaurant/${restaurantId}/deactivate`)
  }

  deleteRestaurantById (restaurantId) {
    this.setHeader(getAuthToken())
    return this.delete(`/v1/admin/restaurant/${restaurantId}`)
  }

  // createPackage (payload) {
  //   this.setHeader(getAuthToken())
  //   return this.post('/v1/admin/package', payload)
  // }

  // updatePackage (id, payload) {
  //   this.setHeader(getAuthToken())
  //   return this.put(`/v1/admin/package/${id}`, payload)
  // }

  // deletePackage (id) {
  //   this.setHeader(getAuthToken())
  //   return this.delete(`/v1/admin/package/${id}`)
  // }
}

export default new RestaurantProvider()
