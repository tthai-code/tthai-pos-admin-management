import { getAuthToken } from '../utils/auth'
import HttpRequest from './HttpRequest'

class BillingProvider extends HttpRequest {
  getBilling (query) {
    this.setHeader(getAuthToken())
    return this.get('/v1/admin/bill', query)
  }

  getBillingById (id) {
    // this.setHeader(getAuthToken())
    return this.get(`/v1/admin/bill/${id}/billing`)
  }

  createLastMonthBilling (startedPeriod, endedPeriod, invoiceDate) {
    this.setHeader(getAuthToken())
    return this.post('/v1/cron/bill', {
      startedPeriod, endedPeriod, invoiceDate
    })
  }

  payLastMonthBilling (invoiceDate) {
    this.setHeader(getAuthToken())
    return this.post('/v1/cron/bill/pay', { invoiceDate
    })
  }
}

export default new BillingProvider()
