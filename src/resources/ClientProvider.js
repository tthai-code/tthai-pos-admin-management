import { getAuthToken } from '../utils/auth'
import HttpRequest from './HttpRequest'

class ClientProvider extends HttpRequest {
  getClients (query) {
    this.setHeader(getAuthToken())

    return this.get('/client', query)
  }

  getClientById (id) {
    this.setHeader(getAuthToken())
    return this.get(`/client/${id}`)
  }

  createClient (payload) {
    this.setHeader(getAuthToken())

    return this.post('/client', payload)
  }

  updateClient (id, payload) {
    this.setHeader(getAuthToken())
    return this.put(`/client/${id}`, payload)
  }

  deleteClient (id) {
    this.setHeader(getAuthToken())
    return this.delete(`/client?id=${id}`)
  }
}

export default ClientProvider
