import { getAuthToken } from '../utils/auth'
import HttpRequest from './HttpRequest'

class AdminProvider extends HttpRequest {
  getAdmins (query) {
    this.setHeader(getAuthToken())
    return this.get('/v1/users', query)
  }

  getAdminById (id) {
    this.setHeader(getAuthToken())
    return this.get(`/v1/users/${id}`)
  }

  createAdmin (payload) {
    this.setHeader(getAuthToken())
    return this.post('/v1/users', payload)
  }

  updateAdmin (id, payload) {
    this.setHeader(getAuthToken())
    return this.put(`/v1/users/${id}`, payload)
  }

  deleteAdmin (id) {
    this.setHeader(getAuthToken())
    return this.delete(`/v1/users?id=${id}`)
  }

  changePassword (id, payload) {
    this.setHeader(getAuthToken())
    return this.delete(`/v1/users/${id}/password`, payload)
  }
}

export default AdminProvider
