import { getAuthToken } from '../utils/auth'
import HttpRequest from './HttpRequest'

class PackageProvider extends HttpRequest {
  getPackage (query) {
    this.setHeader(getAuthToken())
    return this.get('/v1/admin/package', query)
  }

  getPackageById (id) {
    this.setHeader(getAuthToken())
    return this.get(`/v1/admin/package/${id}`)
  }

  createPackage (payload) {
    this.setHeader(getAuthToken())
    return this.post('/v1/admin/package', payload)
  }

  updatePackage (id, payload) {
    this.setHeader(getAuthToken())
    return this.put(`/v1/admin/package/${id}`, payload)
  }

  deletePackage (id) {
    this.setHeader(getAuthToken())
    return this.delete(`/v1/admin/package/${id}`)
  }
}

export default new PackageProvider()
