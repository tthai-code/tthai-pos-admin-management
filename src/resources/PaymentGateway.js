import { getAuthToken } from '../utils/auth'
import HttpRequest from './HttpRequest'

class PaymentGatewayProvider extends HttpRequest {
  getPaymentGatewayStatus (restaurantId) {
    this.setHeader(getAuthToken())
    return this.get(`/v1/admin/payment-gateway/status?restaurantId=${restaurantId}`)
  }

  getCoPilotAppStatus (restaurantId) {
    this.setHeader(getAuthToken())
    return this.get(`/v1/admin/payment-gateway/co-pilot?restaurantId=${restaurantId}`)
  }

  getPaymentGatewaySetting (restaurantId) {
    this.setHeader(getAuthToken())
    return this.get(`/v1/admin/payment-gateway/${restaurantId}`)
  }

  updateCoPilotStatus (restaurantId, payload) {
    this.setHeader(getAuthToken())
    return this.patch(`/v1/admin/payment-gateway/co-pilot?restaurantId=${restaurantId}`, payload)
  }

  updateBankAccountStatus (restaurantId, payload) {
    this.setHeader(getAuthToken())
    return this.patch(`/v1/admin/payment-gateway/bank-account?restaurantId=${restaurantId}`, payload)
  }

  updatePaymentGatewaySetting (restaurantId, payload) {
    this.setHeader(getAuthToken())
    return this.put(`/v1/admin/payment-gateway/${restaurantId}`, payload)
  }
}

export default new PaymentGatewayProvider()
