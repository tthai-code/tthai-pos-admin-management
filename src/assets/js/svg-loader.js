function importAll (r, prefix = '') {
  console.log('r', r)
  const components = {}
  r.keys().forEach((key) => {
    const component = r(key)
    const name = key.toString().toLowerCase().replace('./', '').replace('.svg', '')
    components[`${prefix}-${name}`] = {
      component
    }
  })

  console.log('components', components)
  return components
}

export default importAll
