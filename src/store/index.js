import Vue from 'vue'
import Vuex from 'vuex'
import { vuexUser } from '@/plugins/VuexPersist'

import UserModule from './user'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    User: UserModule
  },
  plugins: [
    vuexUser.plugin
  ]
})

export default store
