import AuthProvider from '@/resources/AuthProvider'
import Base64 from '@/plugins/Base64'

const authService = new AuthProvider()

const state = {
  accessToken: null,
  id: null,
  username: null,
  tokenExpire: null,
  client: {
    email: null,
    id: null,
    name: null,
    tel: null
  },
  role: null,
  remember: {
    username: null,
    password: null
  }
}

const actions = {
  async signin ({ dispatch }, payload) {
    const res = await authService.login({
      username: payload.username,
      password: payload.password
    })

    if (res.accessToken) {
      dispatch('setUser', {
        ...res.data,
        accessToken: res.accessToken
      })
      if (payload.isRemember) {
        dispatch('setRemember', {
          username: payload.username,
          password: payload.password
        })
      } else {
        dispatch('resetRemember')
      }
    }
  },
  setUser ({ commit }, payload) {
    commit('SET_USER', payload)
  },
  setRemember ({ commit }, payload) {
    commit('SET_REMEMBER', {
      username: payload.username,
      password: Base64.encodeByKey(payload.password, 'P@s$w0rd')
    })
  },
  resetRemember ({ commit }) {
    commit('RESET_REMEMBER')
  },
  resetUser ({ commit }) {
    commit('RESET_USER')
  }
}

const mutations = {
  SET_USER (state, payload) {
    state.accessToken = payload?.accessToken || null
    state.id = payload?.id || null
    state.username = payload?.username || null
    state.tokenExpire = payload?.tokenExpire || null
    state.client.email = payload?.client?.email || null
    state.client.id = payload?.client?.id || null
    state.client.name = payload?.client?.name || null
    state.client.tel = payload?.client?.tel || null
    state.role = payload?.role || null
  },
  SET_REMEMBER (state, payload) {
    state.remember.username = payload?.username || null
    state.remember.password = payload?.password || null
  },
  RESET_USER (state) {
    state.accessToken = null
    state.id = null
    state.username = null
    state.tokenExpire = null
    state.client.email = null
    state.client.id = null
    state.client.name = null
    state.client.tel = null
  },
  RESET_REMEMBER (state) {
    state.remember.username = null
    state.remember.password = null
  }
}

const getters = {
  clientData: (state) => state.client,
  clientId: (state) => state.client.id,
  accessToken: (state) => state.accessToken,
  tokenExpire: (state) => state.tokenExpire,
  role: (state) => state.role,
  username: (state) => state.username,
  remember: (state) => {
    const { remember } = state
    if (remember.username && remember.password) {
      return {
        username: remember.username,
        password: Base64.decodeByKey(remember.password, 'P@s$w0rd')
      }
    }
    return {}
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
