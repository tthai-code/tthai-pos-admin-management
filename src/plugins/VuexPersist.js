import VuexPersistence from 'vuex-persist'
import Cookies from 'js-cookie'
import Base64 from './Base64'

const ECODE_KEY = process.env.VUE_APP_ENCODE_KEY || ''

export const vuexUser = new VuexPersistence({
  key: '_tthai-pos_user',
  restoreState: (key) => {
    let cookieData = Cookies.get(key) || {}
    let localStorageData = localStorage.getItem(key) || {}
    if (cookieData && typeof cookieData === 'string') {
      cookieData = JSON.parse(Base64.decodeByKey(cookieData, ECODE_KEY))
    }
    if (localStorageData && typeof localStorageData === 'string') {
      localStorageData = JSON.parse(Base64.decodeByKey(localStorageData, ECODE_KEY))
    }

    console.log('user', {
      ...cookieData,
      ...localStorageData
    })
    return {
      User: {
        ...cookieData,
        ...localStorageData
      }
    }
  },
  saveState: (key, state) => {
    const tokenExpire = state?.User?.tokenExpire
    const expires = tokenExpire ? new Date(tokenExpire) : 6
    const cookieData = {
      accessToken: state?.User?.accessToken || null,
      id: state?.User?.id || null,
      username: state?.User?.username || null,
      role: state?.User?.role || null,
      tokenExpire: state?.User?.tokenExpire || null,
      client: {
        email: state?.User?.client?.email || null,
        id: state?.User?.client?.id || null,
        name: state?.User?.client?.name || null,
        tel: state?.User?.client?.tel || null
      }
    }
    const localStorageData = {
      remember: {
        username: state?.User?.remember?.username || null,
        password: state?.User?.remember?.password || null
      }
    }
    Cookies.set(key, Base64.encodeByKey(JSON.stringify(cookieData), ECODE_KEY), {
      expires
    })
    localStorage.setItem(key, Base64.encodeByKey(JSON.stringify(localStorageData), ECODE_KEY))
  },
  modules: ['User']
})

export default {
  vuexUser
}
