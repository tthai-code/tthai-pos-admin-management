import Vue from 'vue'

Vue.filter('fullNumber2Decimal', (val) => val?.toLocaleString(undefined, {
  minimumFractionDigits: 2,
  maximumFractionDigits: 2
}))

Vue.filter('fullNumberNoDecimal', (val) => val?.toLocaleString(undefined, {
  minimumFractionDigits: 0,
  maximumFractionDigits: 0
}))
