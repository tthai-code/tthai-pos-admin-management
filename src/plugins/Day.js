import Vue from 'vue'
import dayjs from 'dayjs'

Vue.prototype.$dayjs = dayjs

Vue.filter('slashedDate', (val) => dayjs(val).format('MM/DD/YYYY'))
Vue.filter('dateMiniMonthYear', (val) => dayjs(val).format('DD MMM YYYY'))
